.PHONY: clean protoc setup build grpc

clean:
	@echo "--- Cleanup all build and generated files ---"
	@rm -vf src/infrastructure/grpc/proto/*.pb.go
	@rm -vf src/infrastructure/grpc/proto/movies/*.pb.go
	@rm -vf ./main

protoc: clean
	@echo "--- Preparing proto output directories ---"
	@mkdir -p src/infrastructure/grpc/proto/movies

	@echo "--- Compiling all proto files ---"
	@cd ./src/shared/proto/movies && protoc -I. --go_out=plugins=grpc:../../../../src/infrastructure/grpc/proto/movies --govalidators_out=../../../../src/infrastructure/grpc/proto/movies *.proto
	@gofmt -s -w .

setup:
	@echo "--- Setup and generated NEW Config FILE ---"
	@cp config/example/database.yml config/database/database.yml
	@cp config/example/client.yml config/client/client.yml
	@cp config/example/server.yml config/server/server.yml
	@cp config/example/message.yml config/message/message.yml

build: setup clean protoc
	@echo "---- Setup, Clean and Generate Proto ---"
	@echo "--- Building binary file ---"
	@go build -o ./main src/main.go

grpc:
	@echo "--- running gRPC server in dev mode ---"
	@ go run src/main.go grpc