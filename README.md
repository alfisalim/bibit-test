# BIBIT TEST

****

## Description
This repository contain my result for bibit test-case golang developer.
little bit about this project, i made this project with open 2 gateway,
first gate using grpc architecture and second gate using rest api.
and also i use clean code technique. In project structure, there is 3 layers,
namely service layer, interactor layer and adapter layer.

Service layer used for catch each request client
and then forward the request to the next layer.
this layer also used as receiver return from interactor layers.

Interactor layer handling all about logic on this service,
and then forward ther clearly request to adapter layers.
this layer also used as receiver return from adapter layers.

Adapter layer used for build response from the request, actually this layer
used for handling all about client, either third party, database, or etc.

## How to Use
1. clone this repository:
   ```https://gitlab.com/alfisalim/bibit-test.git```
2. change directory to this project : ```cd bibit-test/```
3. open terminal, cmd, or etc and type this command
    - ```make protoc``` for generate all protobuf file needed
    - ```make setup``` for generate all configuration file
    - ```make clean``` for remove all protobuf file
    - ```make build``` for generate all configuration file, protobuf file and including build binary file go
    - ```make grpc``` for running grpc server
4. we can test every service on this project using tools grpc client like bloom rpc or etc.

## Framework
- GORM (Go Object Relatinal Mapping)
- Resty
- Viper
- GRPC (Go Remote Procedure Call)
- testify and go convey
- protobuf and proto-validator

## Documentation
- Proto Spesification SearchMovies Service

| Request Field  | Type | mandatory| Response Field | Type |
| -------------- | ---- | -------- | -------------- | ---- |
| Keyword | string | yes | | |
| Page    | int64  | no  | | |
|         |        |     | Search repeated | - Title (string) <br>- Year (string) <br>- ImdbID (string) <br>- Type (string) <br>- Poster (string) |
|         |        |     | TotalResult | string |
|         |        |     | Response | string |
|         |        |     | Error | string |


- Proto Spesification DetailMovie Service

| Request Field  | Type | mandatory| Response Field | Type |
| -------------- | ---- | -------- | -------------- | ---- |
| Title | string | yes | | |
| Year    | string  | no  | | |
|         |        |     | Title | string |
|         |        |     | Year | string |
|         |        |     | Rated | string |
|         |        |     | Released | string |
|         |        |     | Runtime | string |
|         |        |     | Genre | string |
|         |        |     | Director | string |
|         |        |     | Writer | string |
|         |        |     | Actors | string |
|         |        |     | Plot | string |
|         |        |     | Language | string |
|         |        |     | Country | string |
|         |        |     | Awards | string |
|         |        |     | Poster | string |
|         |        |     | Ratings repeated | - source (string) <br>- value (string)|
|         |        |     | Metascore | string |
|         |        |     | ImdbRating | string |
|         |        |     | ImdbVotes | string |
|         |        |     | ImdbID | string |
|         |        |     | DVD | string |
|         |        |     | BoxOffice | string |
|         |        |     | Production | string |
|         |        |     | Website | string |
|         |        |     | Response | string |
|         |        |     | Error | string |


****
****

### Result Unit Test
- Unit Test Service Layer

![image info](./soal-test/capture_unit_test/unit_test_movies_service.png)

- Unit Test Interactor Layer

![image info](./soal-test/capture_unit_test/unit_test_movies_interactor.png)

### Example Result Using Bloom
- SearchMovies
  
![image info](./soal-test/capture_unit_test/search_movie_bloom.png)

- DetailMovie

![image info](./soal-test/capture_unit_test/detail_movie_bloom.png)


### Tracing jaeger

![image info](./soal-test/capture_unit_test/jaeger.png)

## Change Log
- 14 Nov 2021 23:31
  - Initial Service

## Author
**ALFI SALIM** | Programmer | **alfisalim.12@gmail.com**
