package client

type ClientList struct {
	Rest Omdb `yaml:"rest"`
}

type Omdb struct {
	Omdb Base `yaml:"omdb"`
}

type Base struct {
	Url string `yaml:"url"`
	Key string `yaml:"key"`
}
