package database

type DatabaseList struct {
	Mysql Mysql `yaml:"mysql"`
}

type Mysql struct {
	MovieDB Base `yaml:"movieDB"`
}

type Base struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
	Schema   string `yaml:"schema"`
}
