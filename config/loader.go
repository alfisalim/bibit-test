package config

import (
	"bibit-test/config/client"
	"bibit-test/config/database"
	"bibit-test/config/message"
	"bibit-test/config/server"
	"encoding/json"
	"fmt"
	"log"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/spf13/viper"
)

type Config struct {
	Database database.DatabaseList `yaml:"database"`
	Client   client.ClientList     `yaml:"client"`
	Server   server.ServerList     `yaml:"server"`
	Message  message.MessageList   `yaml:"Message"`
}

var cfg Config

var (
	_, b, _, _ = runtime.Caller(0)
	basepath   = filepath.Dir(b)
)

func init() {
	var err error

	fmt.Printf("Reading Config %s\n", basepath+"/database")
	viper.AddConfigPath(basepath + "/database")
	viper.SetConfigType("yml")
	viper.SetConfigName("database.yml")
	err = viper.MergeInConfig()
	if err != nil {
		log.Println("Cannot read database config: %v", err)
	}

	fmt.Printf("Reading Config %s\n", basepath+"/client")
	viper.AddConfigPath(basepath + "/client")
	viper.SetConfigName("client.yml")
	err = viper.MergeInConfig()
	if err != nil {
		log.Println("Cannot read client config: %v", err)
	}

	fmt.Printf("Reading Config %s\n", basepath+"/server")
	viper.AddConfigPath(basepath + "/server")
	viper.SetConfigName("server.yml")
	err = viper.MergeInConfig()
	if err != nil {
		log.Println("Cannot read logger config: %v", err)
	}

	fmt.Printf("Reading Config %s\n", basepath+"/message")
	viper.AddConfigPath(basepath + "/message")
	viper.SetConfigName("message.yml")
	err = viper.MergeInConfig()
	if err != nil {
		log.Println("Cannot read logger config: %v", err)
	}

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	viper.Unmarshal(&cfg)

	fmt.Println("=============================")
	fmt.Println(Stringify(cfg))
	fmt.Println("=============================")
}

// GetConfig get config
func GetConfig() *Config {
	return &cfg
}

func Stringify(data interface{}) string {
	dataByte, _ := json.Marshal(data)
	return string(dataByte)
}
