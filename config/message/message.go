package message

type MessageList struct {
	RabbitMQ Base `yaml:"rabbitmq"`
}

type Base struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	Username string `yaml:"username"`
	Password string `yaml:"password"`
}
