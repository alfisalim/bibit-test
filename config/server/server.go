package server

type ServerList struct {
	Grpc Base `yaml:"grpc"`
}

type Base struct {
	Host    string `yaml:"host"`
	Port    string `yaml:"port"`
	Timeout int    `yaml:"timeout"`
}
