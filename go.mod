module bibit-test

go 1.16

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.1.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0 // indirect
	github.com/mitchellh/mapstructure v1.4.2
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/opentracing/opentracing-go v1.1.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/smartystreets/goconvey v1.7.2
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	github.com/uber/jaeger-client-go v2.30.0+incompatible // indirect
	github.com/uber/jaeger-lib v2.4.1+incompatible // indirect
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.3
)
