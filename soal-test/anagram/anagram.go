package main

import (
	"fmt"
	"sort"
	"strings"
)

func anagramClassification(slice []string) (result [][]string) {
	newList := map[string][]string{}

	for _, value := range slice {
		key := sortString(value)
		newList[key] = append(newList[key], value)
	}

	for _, value := range newList {
		result = append(result, value)
	}

	return
}

func sortString(str string) string {
	s := strings.Split(str, "")
	sort.Strings(s)

	return strings.Join(s, "")
}

func main() {
	slice := []string{"kita", "atik", "tika", "aku", "kia", "makan", "kua"}
	res := anagramClassification(slice)

	fmt.Println(res)
}
