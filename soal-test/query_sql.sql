CREATE DATABASE bibit_db;

use bibit_db;

create table user (
    id int(10) auto_increment primary key,
    UserName varchar(20) default null,
    parent int(10) default null
) engine=InnoDB;

create table parent (
    id int(10) primary key,
    ParentName varchar(20) default null
) engine=InnoDB;

insert into user (Username, parent) values("Ali", 2), ("Budi", 0), ("Cecep", 1);

insert into parent (id, ParentName) values(0, null), (1, "Ali"), (2, "Budi");

select a.id as ID, a.Username as UserName, b.ParentName as ParentUserName from user as a
    left join parent as b on a.parent = b.id;