package main

import (
	"fmt"
	"strings"
)

/*
============================================================================================
FORM THIS CODE
============================================================================================
*/

func findFirstStringInBracket(str string) string {
	if len(str) > 0 {
		indexFirstBracketFound := strings.Index(str, "(")
		if indexFirstBracketFound >= 0 {
			runes := []rune(str)
			wordsAfterFirstBracket := string(runes[indexFirstBracketFound:len(str)])
			indexClosingBracketFound := strings.Index(wordsAfterFirstBracket, ")")
			if indexClosingBracketFound >= 0 {
				runes := []rune(wordsAfterFirstBracket)
				return string(runes[1 : indexClosingBracketFound-1])
			} else {
				return ""
			}
		} else {
			return ""
		}
	} else {
		return ""
	}
	return ""
}

/*
============================================================================================
TO THIS CODE
============================================================================================
*/

func newFindFirstString(str string) string {
	stringCounter := len(str)

	if stringCounter == 0 {
		return ""
	}

	if indexFirstBracket := strings.Index(str, "("); indexFirstBracket >= 0 {
		wordsAfterFirstBracket := str[indexFirstBracket:]
		if indexClosingBracket := strings.Index(wordsAfterFirstBracket, ")"); indexClosingBracket >= 0 {
			return wordsAfterFirstBracket[1:indexClosingBracket]
		}
		return ""
	}
	return ""
}

func main() {
	str := "ini( adalah ibu) budi"
	newStr := newFindFirstString(str)
	newStr2 := findFirstStringInBracket(str)

	fmt.Println("str = ", newStr)
	fmt.Println("str new = ", newStr2)
}
