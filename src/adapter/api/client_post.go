package api

import (
	"bibit-test/config"
	"bibit-test/src/adapter/repository"
	"bibit-test/src/entity"
	"bibit-test/src/shared/util"
	"encoding/json"
	"github.com/go-resty/resty/v2"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"os"
	"time"
)

type BuilderClient struct {
	url        string
	parameters map[string]string
	db         repository.LoggerDBRepository
}

func NewBuilderClient(url, key string, db repository.LoggerDBRepository) *BuilderClient {
	return &BuilderClient{
		url: url,
		parameters: map[string]string{
			"apikey": key,
		},
		db: db,
	}
}

func (p *BuilderClient) SetParameter(req map[string]string) *BuilderClient {
	for key, val := range req {
		p.parameters[key] = val
	}

	return p
}

func (p *BuilderClient) Post(out interface{}) error {
	conf := config.GetConfig()
	timeout := conf.Server.Grpc.Timeout

	cli := resty.New()
	cli.SetTimeout(time.Duration(timeout) * time.Millisecond)

	host, _ := os.Hostname() //get server hitter

	//base data logger
	LogApi := &entity.FieldDbLogger{
		Url:     p.url,
		Client:  host,
		Request: util.ConvertToBase64(p.parameters),
	}

	response, err := cli.R().SetQueryParams(p.parameters).Get(p.url)
	if err != nil {
		mapstructure.Decode(response, out)
		LogApi.Response = util.ConvertToBase64(out)
		go p.db.InsertLog(LogApi)
		return status.Error(codes.Internal, err.Error())
	}

	//if response.StatusCode() != 200 {
	//	errRes := fmt.Errorf("Client %s", response.Status())
	//	return status.Error(codes.Internal, errRes.Error())
	//}

	var body = response.Body()
	if err = json.Unmarshal(body, out); err != nil {
		LogApi.Response = util.ConvertToBase64(out)
		go p.db.InsertLog(LogApi)
		return status.Error(codes.Internal, err.Error())
	}

	LogApi.Response = util.ConvertToBase64(out)
	go p.db.InsertLog(LogApi)

	return nil
}