package api

import (
	repository2 "bibit-test/src/adapter/repository"
	"bibit-test/src/entity"
	"bibit-test/src/infrastructure/grpc/logging"
	"github.com/opentracing/opentracing-go"
)

type MoviesClient struct {
	client *BuilderClient
}

func NewMoviesClient(client *BuilderClient) repository2.MoviesRepository {
	return &MoviesClient{
		client: client,
	}
}

func (m *MoviesClient) SearchMovies(span opentracing.Span, i interface{}) (interface{}, error) {
	sp := logging.CreateSubChildSpan(span, "Search Movies Client")
	defer sp.Finish()

	req := i.(map[string]string)
	logging.LogRequest(sp, req)

	m.client.SetParameter(req)

	var response = new(entity.MoviesResponse)
	err := m.client.Post(response)
	if err != nil {
		logging.LogError(sp, err)
		return nil, err
	}

	logging.LogResponse(sp, response)
	return response, nil
}

func (m *MoviesClient) DetailMovie(i interface{}) (interface{}, error) {
	req := i.(map[string]string)
	m.client.SetParameter(req)

	var response = new(entity.DetailMovieResponse)
	err := m.client.Post(response)
	if err != nil {
		return nil, err
	}

	return response, nil
}
