package database

import (
	"bibit-test/config/database"
	"bibit-test/src/entity"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"time"
)

//type DatabaseConnection struct{}

func NewDatabaseConnection(config database.Base) *gorm.DB {

	username := config.Username
	password := config.Password
	host := config.Host
	port := config.Port
	dbname := config.Schema

	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		username, password, host, port, dbname)

	db, err := gorm.Open(mysql.Open(dsn))
	if err != nil {
		log.Fatalln(err)
	}

	db.AutoMigrate(&entity.FieldDbLogger{})

	sqlDB, _ := db.DB()

	sqlDB.SetMaxIdleConns(10)
	sqlDB.SetMaxOpenConns(10)
	sqlDB.SetConnMaxLifetime(1 * time.Hour)

	return db
}
