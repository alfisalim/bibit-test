package database

import (
	"bibit-test/config"
	"bibit-test/src/entity"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"gorm.io/gorm"
)

type LoggerDatabase struct {
	db *gorm.DB
}

func NewLoggerDatabase() *LoggerDatabase {
	conf := config.GetConfig()

	db := NewDatabaseConnection(conf.Database.Mysql.MovieDB)
	return &LoggerDatabase{
		db: db,
	}
}

func (l *LoggerDatabase) InsertLog(i interface{}) error {
	var data *entity.FieldDbLogger
	err := mapstructure.Decode(i, &data)
	if err != nil {
		return status.Error(codes.Internal, err.Error())
	}

	query := l.db.Debug().Create(&data)
	if query.Error != nil {
		return query.Error
	}

	return nil
}



