package repository

import "github.com/opentracing/opentracing-go"

type MoviesRepository interface {
	SearchMovies(span opentracing.Span, i interface{}) (interface{}, error)
	DetailMovie(interface{}) (interface{}, error)
}
