package entity

import "time"

type FieldDbLogger struct {
	Id        int64      `gorm:"primary_key;auto_increment;not_null"`
	Url       string     `gorm:"type:varchar(200);not null"`
	Request   string     `gorm:"type:text"`
	Header    string     `gorm:"type:varchar(2000)"`
	Response  string     `gorm:"type:text;not null"`
	Client    string     `gorm:"type:varchar(200);not null"`
	CreatedAt *time.Time `gorm:"type:datetime;not null"`
}

func (FieldDbLogger) TableName() string {
	return "logging"
}
