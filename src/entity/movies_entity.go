package entity

type MoviesResponse struct {
	Search      []*MoviesData `json:"Search"`
	TotalResult string        `json:"totalResults"`
	Response    string        `json:"Response"`
	Error       string        `json:"Error"`
}

type MoviesData struct {
	Title  string `json:"Title"`
	Year   string `json:"Year"`
	ImdbID string `json:"ImdbID"`
	Type   string `json:"Type"`
	Poster string `json:"Poster"`
}

type SearchMoviesRequest struct {
	Keyword   string
	Page      int64
	ResFormat string
}

type DetailMovieRequest struct {
	Title string
	Year  string
}

type RatingsData struct {
	Source string `json:"Source,omitempty"`
	Value  string `json:"Value,omitempty"`
}

type DetailMovieResponse struct {
	Title      string         `json:"Title,omitempty"`
	Year       string         `json:"Year,omitempty"`
	Rated      string         `json:"Rated,omitempty"`
	Released   string         `json:"Released,omitempty"`
	Runtime    string         `json:"Runtime,omitempty"`
	Genre      string         `json:"Genre,omitempty"`
	Director   string         `json:"Director,omitempty"`
	Writer     string         `json:"Writer,omitempty"`
	Actors     string         `json:"Actors,omitempty"`
	Plot       string         `json:"Plot,omitempty"`
	Language   string         `json:"Language,omitempty"`
	Country    string         `json:"Country,omitempty"`
	Awards     string         `json:"Awards,omitempty"`
	Poster     string         `json:"Poster,omitempty"`
	Ratings    []*RatingsData `json:"Ratings,omitempty"`
	MetaScore  string         `json:"MetaScore,omitempty"`
	ImdbRating string         `json:"ImdbRating,omitempty"`
	ImdbVotes  string         `json:"ImdbVotes,omitempty"`
	ImdbID     string         `json:"ImdbID,omitempty"`
	Type       string         `json:"Type,omitempty"`
	DVD        string         `json:"DVD,omitempty"`
	BoxOffice  string         `json:"BoxOffice,omitempty"`
	Production string         `json:"Production,omitempty"`
	Website    string         `json:"Website,omitempty"`
	Response   string         `json:"Response,omitempty"`
	Error      string         `json:"Error,omitempty"`
}
