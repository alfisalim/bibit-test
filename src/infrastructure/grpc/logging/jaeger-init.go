package logging

import (
	"bibit-test/src/shared/util"
	"context"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/log"
	"github.com/uber/jaeger-client-go"
	jaegerConf "github.com/uber/jaeger-client-go/config"
	jaegerLog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics"
	"io"
)

func Init(serviceName string) (opentracing.Tracer, io.Closer) {
	cfg := jaegerConf.Configuration{
		ServiceName: "bibit-test",
		Sampler: &jaegerConf.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegerConf.ReporterConfig{
			LogSpans: true,
		},
	}

	jLogger := jaegerLog.StdLogger
	jMetricsFactory := metrics.NullFactory

	tracer, closer, err := cfg.NewTracer(
		jaegerConf.Logger(jLogger),
		jaegerConf.Metrics(jMetricsFactory),
	)

	if err != nil {
		panic(err)
	}

	opentracing.SetGlobalTracer(tracer)
	//defer closer.Close()

	return tracer, closer
}

func StartRootSpan(ctx context.Context, name string) (context.Context, io.Closer, opentracing.Span) {
	init, closer := Init(name)
	sp := init.StartSpan("mnc-service")
	ctx = opentracing.ContextWithSpan(ctx, sp)
	return ctx, closer, sp
}

func CreateRootSpan(ctx context.Context, serviceName string) (opentracing.Span, opentracing.Tracer) {
	parentSpan, _ := opentracing.StartSpanFromContext(ctx, serviceName)

	fmt.Println("=============================== ", parentSpan)

	tracer := parentSpan.Tracer()
	return parentSpan, tracer
}

func CreateChildSpan(parentSpan opentracing.Span, name string) opentracing.Span {
	sp := opentracing.StartSpan(
		name,
		opentracing.ChildOf(parentSpan.Context()))
	sp.SetTag("name", name)

	return sp
}

func CreateSubChildSpan(childSpan opentracing.Span, name string) opentracing.Span {
	sp := opentracing.StartSpan(
		name,
		opentracing.ChildOf(childSpan.Context()))
	sp.SetTag("name", name)

	return sp
}

func LogRequest(sp opentracing.Span, request interface{}) {
	sp.LogFields(log.Object("Request", util.Stringify(request)))
}

func LogResponse(sp opentracing.Span, response interface{}) {
	sp.LogFields(log.Object("Response", util.Stringify(response)))
}

func LogObject(sp opentracing.Span, name string, obj interface{}) {
	sp.LogFields(log.Object(name, util.Stringify(obj)))
}

func LogError(sp opentracing.Span, err error) {
	sp.LogFields(log.Object("Error", err))
}
