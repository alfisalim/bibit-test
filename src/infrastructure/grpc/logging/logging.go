package logging

import (
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc"
)

func LoggingServerInterceptor() grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		_, _ = uuid.NewUUID()
		return nil, nil
	}

}
