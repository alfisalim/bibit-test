package main

import (
	"bibit-test/config"
	api2 "bibit-test/src/adapter/api"
	"bibit-test/src/adapter/database"
	"bibit-test/src/infrastructure/grpc/logging"
	movies3 "bibit-test/src/infrastructure/grpc/proto/movies"
	service2 "bibit-test/src/service"
	movies2 "bibit-test/src/usecase/movies"
	"context"
	"encoding/json"
	"fmt"
	"github.com/opentracing/opentracing-go"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	conf := config.GetConfig()
	svcPort := conf.Server.Grpc.Port

	tracer, closer := logging.Init("bibit-test")
	defer closer.Close()

	opentracing.SetGlobalTracer(tracer)

	grpcServer := grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			GrpcLogging,
		),
	)

	Apply(grpcServer)

	l, err := net.Listen("tcp", fmt.Sprintf(":%s", svcPort))
	if err != nil {
		log.Fatalf("could not listen to %s: %v", svcPort, err)
	}

	fmt.Printf("Server running at port :%s", svcPort)
	log.Fatal(grpcServer.Serve(l))
}

func Apply(server *grpc.Server) {
	conf := config.GetConfig()
	confOmdb := conf.Client.Rest.Omdb

	movieDB := database.NewLoggerDatabase()
	movileClientBuilder := api2.NewBuilderClient(confOmdb.Url, confOmdb.Key, movieDB)
	movieClient := api2.NewMoviesClient(movileClientBuilder)
	movieUc := movies2.NewMoviesInteractor(movieClient)
	movieService := service2.NewMoviesService(movieUc)

	movies3.RegisterMoviesServiceServer(server, movieService)
}

// GrpcLogging is a gRPC UnaryServerInterceptor that will log the API call to stdOut
func GrpcLogging(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (response interface{}, err error) {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	request, _ := json.Marshal(req)
	logger.Info("request: ", string(request))

	res, err := handler(ctx, req)
	if err != nil {
		logger.Error("error: ", err)
		return nil, err
	}

	resp, _ := json.Marshal(res)
	logger.Info("response: ", string(resp))

	return res, nil
}
