package service

import (
	"bibit-test/src/infrastructure/grpc/logging"
	movies2 "bibit-test/src/infrastructure/grpc/proto/movies"
	"bibit-test/src/usecase/movies"
	"context"
	"github.com/mitchellh/mapstructure"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type MoviesService struct {
	moviesRepo movies.MoviesPort
}

func NewMoviesService(moviesRepo movies.MoviesPort) *MoviesService {
	return &MoviesService{
		moviesRepo: moviesRepo,
	}
}

func (m *MoviesService) SearchMovies(ctx context.Context, request *movies2.SearchMoviesRequest) (*movies2.SearchMoviesResponse, error) {
	sp, _ := logging.CreateRootSpan(ctx, "SearchMovies Services")
	defer sp.Finish()

	logging.LogRequest(sp, request)

	res, err := m.moviesRepo.SearchMovies(sp, request)
	if err != nil {
		logging.LogError(sp, err)
		return nil, err
	}

	logging.LogObject(sp, "RESPONSE FROM INTERACTOR LAYER", res)

	var resp *movies2.SearchMoviesResponse
	err = mapstructure.Decode(res, &resp)
	if err != nil {
		logging.LogError(sp, err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	logging.LogResponse(sp, resp)
	return resp, nil
}

func (m *MoviesService) DetailMovie(ctx context.Context, request *movies2.DetailMovieRequest) (*movies2.DetailMovieResponse, error) {
	res, err := m.moviesRepo.DetailMovie(request)
	if err != nil {
		return nil, err
	}

	var resp *movies2.DetailMovieResponse
	err = mapstructure.Decode(res, &resp)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return resp, nil
}
