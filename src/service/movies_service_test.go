package service

import (
	movies2 "bibit-test/src/infrastructure/grpc/proto/movies"
	"bibit-test/src/shared/mock/port"
	"context"
	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"testing"
)

func TestSearchMovies(t *testing.T) {
	Convey("SearchMovies Test", t, func() {
		Convey("Positive Scenarios", func() {
			mockMoviesport := port.MockMoviesPort{}

			req := &movies2.SearchMoviesRequest{
				Page:    1,
				Keyword: "batman",
			}

			var datas []*movies2.MoviesData
			data := &movies2.MoviesData{
				Title:  "Batman",
				Year:   "2020",
				ImdbID: "-",
				Type:   "action",
				Poster: "-",
			}

			datas = append(datas, data)

			res := &movies2.SearchMoviesResponse{
				Search:      datas,
				TotalResult: "470",
				Response:    "true",
				Error:       "",
			}

			Convey("When SearchMovies successfully, Should return data", func() {
				mockMoviesport.On("SearchMovies", req).Return(res, nil)
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.SearchMovies(context.Background(), req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("Negative Scenarios", func() {
			mockMoviesport := port.MockMoviesPort{}

			req := &movies2.SearchMoviesRequest{
				Page:    1,
				Keyword: "batman",
			}

			Convey("When SearchMovies failed, Should return error", func() {
				mockMoviesport.On("SearchMovies", mock.Anything).Return(nil, errors.New("Some Error"))
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.SearchMovies(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build response SearchMovies failed, Should return error", func() {
				mockMoviesport.On("SearchMovies", mock.Anything).Return([]string{"some data"}, nil)
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.SearchMovies(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestDetailMovie(t *testing.T) {
	Convey("DetailMovie Test", t, func() {
		Convey("Positive Scenarios", func() {
			mockMoviesport := port.MockMoviesPort{}

			req := &movies2.DetailMovieRequest{
				Title: "batman",
				Year:  "",
			}

			var datas []*movies2.RatingsData
			data := &movies2.RatingsData{
				Source: "TEST",
				Value:  "TEST",
			}

			datas = append(datas, data)

			res := &movies2.DetailMovieResponse{
				Title:      "Batman",
				Year:       "1989",
				Rated:      "PG-13",
				Released:   "23 Jun 1989",
				Runtime:    "126 min",
				Genre:      "Action, Adventure",
				Director:   "Tim Burton",
				Actors:     "Michael Keaton, Jack Nicholson, Kim Basinger",
				Plot:       "The Dark Knight of Gotham City begins his war on crime with his first major enemy being Jack Napier, a criminal who becomes the clownishly homicidal Joker.",
				Language:   "English, French, Spanish",
				Country:    "United States, United Kingdom",
				Awards:     "Won 1 Oscar. 9 wins & 26 nominations total",
				Poster:     "https://m.media-amazon.com/images/M/MV5BMTYwNjAyODIyMF5BMl5BanBnXkFtZTYwNDMwMDk2._V1_SX300.jpg",
				MetaScore:  "69",
				ImdbRating: "7.5",
				ImdbVotes:  "352,984",
				ImdbID:     "tt0096895",
				Type:       "movie",
				DVD:        "22 Aug 1997",
				BoxOffice:  "$251,348,343",
				Production: "N/A",
				Website:    "N/A",
				Response:   "True",
				Error:      "",
			}

			Convey("When DetailMovie successfully, Should return data", func() {
				mockMoviesport.On("DetailMovie", req).Return(res, nil)
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.DetailMovie(context.Background(), req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("Negative Scenarios", func() {
			mockMoviesport := port.MockMoviesPort{}

			req := &movies2.DetailMovieRequest{
				Title: "batman",
				Year:  "",
			}

			Convey("When DetailMovie failed, Should return error", func() {
				mockMoviesport.On("DetailMovie", mock.Anything).Return(nil, errors.New("Some Error"))
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.DetailMovie(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build response DetailMovie failed, Should return error", func() {
				mockMoviesport.On("DetailMovie", mock.Anything).Return([]string{"some data"}, nil)
				svc := NewMoviesService(&mockMoviesport)
				res, err := svc.DetailMovie(context.Background(), req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
