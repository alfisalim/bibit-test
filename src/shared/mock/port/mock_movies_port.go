package port

import "github.com/stretchr/testify/mock"

type MockMoviesPort struct {
	mock.Mock
}

func (m *MockMoviesPort) SearchMovies(i interface{}) (interface{}, error) {
	call := m.Called(i)

	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}

	return res, nil
}

func (m *MockMoviesPort) DetailMovie(i interface{}) (interface{}, error) {
	call := m.Called(i)

	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}

	return res, nil
}
