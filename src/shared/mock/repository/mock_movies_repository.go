package repository

import "github.com/stretchr/testify/mock"

type MockMoviesRepository struct {
	mock.Mock
}

func (m *MockMoviesRepository) SearchMovies(i interface{}) (interface{}, error) {
	call := m.Called(i)

	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}

	return res, nil
}

func (m *MockMoviesRepository) DetailMovie(i interface{}) (interface{}, error) {
	call := m.Called(i)

	res := call.Get(0)
	if res == nil {
		return nil, call.Error(1)
	}

	return res, nil
}

