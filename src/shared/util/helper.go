package util

import (
	"bytes"
	"encoding/base64"
	"encoding/gob"
	"encoding/json"
	"fmt"
)

func ConvertToBase64(in interface{}) string {
	if in == nil {
		return ""
	}

	str, err := json.Marshal(in)
	if err != nil {
		return ""
	}

	b := bytes.Buffer{}
	enc := gob.NewEncoder(&b)
	err = enc.Encode(str)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	str64 := base64.StdEncoding.EncodeToString(b.Bytes())
	return str64
}

func Stringify(data interface{}) string {
	dataByte, _ := json.Marshal(data)
	return string(dataByte)
}
