package movies

import (
	repository2 "bibit-test/src/adapter/repository"
	entity2 "bibit-test/src/entity"
	"bibit-test/src/infrastructure/grpc/logging"
	"errors"
	"github.com/mitchellh/mapstructure"
	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"strconv"
)

type MoviesInteractor struct {
	moviesRepo repository2.MoviesRepository
}

func NewMoviesInteractor(moviesRepo repository2.MoviesRepository) MoviesPort {
	return &MoviesInteractor{
		moviesRepo: moviesRepo,
	}
}

func (m *MoviesInteractor) SearchMovies(span opentracing.Span, i interface{}) (interface{}, error) {
	sp := logging.CreateChildSpan(span, "Search Movies Interactor")
	defer sp.Finish()

	logging.LogObject(sp, "VALUE PARAMS", i)

	if i == nil {
		logging.LogRequest(sp, errors.New("Invalid Request | Request is nil"))
		return nil, status.Error(codes.FailedPrecondition, "Request is nil")
	}

	var request *entity2.SearchMoviesRequest
	err := mapstructure.Decode(i, &request)
	if err != nil {
		logging.LogError(sp, err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	page := strconv.Itoa(int(request.Page))
	req := map[string]string{
		"s":    request.Keyword,
		"page": page,
	}

	logging.LogRequest(sp, req)

	res, err := m.moviesRepo.SearchMovies(sp, req)
	if err != nil {
		logging.LogError(sp, err)
		return nil, err
	}

	logging.LogObject(sp, "RESPONSE FROM ADAPTER LAYER", res)

	var resp *entity2.MoviesResponse
	err = mapstructure.Decode(res, &resp)
	if err != nil {
		logging.LogError(sp, err)
		return nil, status.Error(codes.Internal, err.Error())
	}

	logging.LogResponse(sp, resp)
	return resp, nil
}

func (m *MoviesInteractor) DetailMovie(i interface{}) (interface{}, error) {
	if i == nil {
		return nil, status.Error(codes.FailedPrecondition, "Request is nil")
	}

	var request *entity2.DetailMovieRequest
	err := mapstructure.Decode(i, &request)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	req := map[string]string{
		"t": request.Title,
		"y": request.Year,
	}

	res, err := m.moviesRepo.DetailMovie(req)
	if err != nil {
		return nil, err
	}

	var resp *entity2.DetailMovieResponse
	err = mapstructure.Decode(res, &resp)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return resp, nil
}
