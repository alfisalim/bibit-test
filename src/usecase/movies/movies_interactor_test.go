package movies

import (
	"bibit-test/src/entity"
	movies2 "bibit-test/src/infrastructure/grpc/proto/movies"
	"bibit-test/src/shared/mock/repository"
	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"strconv"
	"testing"
)

func TestSearchMovies(t *testing.T) {
	Convey("SearchMovies Test Interacotr", t, func() {
		Convey("Positive Scenarios", func() {
			mockMoviesport := repository.MockMoviesRepository{}

			req := &entity.SearchMoviesRequest{
				Page:    1,
				Keyword: "batman",
			}

			var datas []*entity.MoviesData
			data := &entity.MoviesData{
				Title:  "Batman",
				Year:   "2020",
				ImdbID: "-",
				Type:   "action",
				Poster: "-",
			}

			datas = append(datas, data)

			res := &entity.MoviesResponse{
				Search:      datas,
				TotalResult: "470",
				Response:    "true",
				Error:       "",
			}

			page := strconv.Itoa(int(req.Page))
			request := map[string]string{
				"s":    req.Keyword,
				"page": page,
			}

			Convey("When SearchMovies successfully, Should return data", func() {
				mockMoviesport.On("SearchMovies", request).Return(res, nil)
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.SearchMovies(req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("Negative Scenarios", func() {
			mockMoviesport := repository.MockMoviesRepository{}

			req := &entity.SearchMoviesRequest{
				Page:    1,
				Keyword: "batman",
			}

			page := strconv.Itoa(int(req.Page))
			request := map[string]string{
				"s":    req.Keyword,
				"page": page,
			}

			Convey("When request is nil, Should return error", func() {
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.SearchMovies(nil)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build request failed, Should return error", func() {
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.SearchMovies(request)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When SearchMovies failed, Should return error", func() {
				mockMoviesport.On("SearchMovies", request).Return(nil, errors.New("Some Error"))
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.SearchMovies(req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build response SearchMovies failed, Should return error", func() {
				mockMoviesport.On("SearchMovies", mock.Anything).Return([]string{"some data"}, nil)
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.SearchMovies(req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})
	})
}

func TestDetailMovie(t *testing.T) {
	Convey("DetailMovie Test Interacotr", t, func() {
		Convey("Positive Scenarios", func() {
			mockMoviesport := repository.MockMoviesRepository{}

			req := &entity.DetailMovieRequest{
				Title: "batman",
				Year:  "",
			}

			var datas []*movies2.RatingsData
			data := &movies2.RatingsData{
				Source: "TEST",
				Value:  "TEST",
			}

			datas = append(datas, data)

			res := &movies2.DetailMovieResponse{
				Title:      "Batman",
				Year:       "1989",
				Rated:      "PG-13",
				Released:   "23 Jun 1989",
				Runtime:    "126 min",
				Genre:      "Action, Adventure",
				Director:   "Tim Burton",
				Actors:     "Michael Keaton, Jack Nicholson, Kim Basinger",
				Plot:       "The Dark Knight of Gotham City begins his war on crime with his first major enemy being Jack Napier, a criminal who becomes the clownishly homicidal Joker.",
				Language:   "English, French, Spanish",
				Country:    "United States, United Kingdom",
				Awards:     "Won 1 Oscar. 9 wins & 26 nominations total",
				Poster:     "https://m.media-amazon.com/images/M/MV5BMTYwNjAyODIyMF5BMl5BanBnXkFtZTYwNDMwMDk2._V1_SX300.jpg",
				MetaScore:  "69",
				ImdbRating: "7.5",
				ImdbVotes:  "352,984",
				ImdbID:     "tt0096895",
				Type:       "movie",
				DVD:        "22 Aug 1997",
				BoxOffice:  "$251,348,343",
				Production: "N/A",
				Website:    "N/A",
				Response:   "True",
				Error:      "",
			}

			request := map[string]string{
				"t":    req.Title,
				"y": req.Year,
			}

			Convey("When DetailMovie successfully, Should return data", func() {
				mockMoviesport.On("DetailMovie", request).Return(res, nil)
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.DetailMovie(req)
				So(res, ShouldNotBeNil)
				So(err, ShouldBeNil)
			})
		})

		Convey("Negative Scenarios", func() {
			mockMoviesport := repository.MockMoviesRepository{}

			req := &entity.DetailMovieRequest{
				Title: "batman",
				Year:  "",
			}

			request := map[string]string{
				"t":    req.Title,
				"y": req.Year,
			}

			Convey("When request is nil, Should return error", func() {
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.DetailMovie(nil)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build request failed, Should return error", func() {
				mockMoviesport.On("DetailMovie", mock.Anything).Return(nil, errors.New("error"))
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.DetailMovie([]string{"error"})
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When DetailMovie failed, Should return error", func() {
				mockMoviesport.On("DetailMovie", request).Return(nil, errors.New("Some Error"))
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.DetailMovie(req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
			Convey("When build response DetailMovie failed, Should return error", func() {
				mockMoviesport.On("DetailMovie", mock.Anything).Return([]string{"some data"}, nil)
				svc := NewMoviesInteractor(&mockMoviesport)
				res, err := svc.DetailMovie(req)
				So(res, ShouldBeNil)
				So(err, ShouldNotBeNil)
			})
		})
	})
}
