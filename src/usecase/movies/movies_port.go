package movies

import "github.com/opentracing/opentracing-go"

type MoviesPort interface {
	SearchMovies(span opentracing.Span, i interface{}) (interface{}, error)
	DetailMovie(interface{}) (interface{}, error)
}
